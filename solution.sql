-- List the books Authored by Majorie Green
SELECT Title.title FROM Title WHERE author.au_fname='Majorie' AND author.au_lname = 'Green';
-- List the books Authored by Michael O'Leary
SELECT Title.title FROM Title WHERE author.au_fname='Michael';
-- Write the author/s of "The Busy Executives Database Guide"
SELECT Author.au_fname, Author.au_lname FROM Author WHERE Title.title = 'The Busy Executives Database Guide';
-- Identify the publisher of "But Is It User Friendly"
SELECT Publisher.pub_id, Publisher.pub_name FROM Publisher WHERE Title.title='But Is It User Friendly';
-- List the books published by Algodata Infosystems.
SELECT Title.title FROM Title WHERE Title.pub_ud='1389';

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(100) NOT NULL,
datetime_create DATETIME NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_user_id
	FOREIGN KEY(author_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE posts_comments(
id INT NOT NULL,
posts_id INT NOT NULL,
users_id INT NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_comments_post
	FOREIGN KEY(posts_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_posts_comments_user
	FOREIGN KEY(users_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE post_likes(
id INT NOT NULL,
posts_id INT NOT NULL,
users_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_likes_users
	FOREIGN KEY(users_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_post_likes_posts
	FOREIGN KEY(posts_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);


